![Alt text](image.png)
# extractor.py
A little (un)known artifact from code inside from **SubSeven 2.1.3 Bonus** client binary, contained a XOR-encrypted version of Hard Drive Killer Pro batch file that would maliciously try to erase files on a hard drive. This code will only decrypt and run if it matched the ICQ number of "7889118" was found in the registry. 
This was written for linux only.



Grab an old version of SubSeven.exe from this repo.
Place the 'SubSeven.exe' file in the same folder as this script before running.
It will automatically decompress the exe, find the RCDATA_3 resource within the binary 
Then it will decrypt that resource using a key of '5075579' and extract to hddkiller.bat.

You may need to:
`apt install upx`
**Usage:**
`python3 extractor.py`




![Alt text](image-1.png)
