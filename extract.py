#!/usr/bin/env python3
# Author: illwill
# Usage:
# Place the 'SubSeven.exe' file in the same folder as this script before running.
# Make sure the name is spelled right.
#
# Example:
#   python3 extractor.py

print(r"""
███████╗██╗   ██╗██████╗ ███████╗                                          
██╔════╝██║   ██║██╔══██╗╚════██║                                          
███████╗██║   ██║██████╔╝    ██╔╝                                          
╚════██║██║   ██║██╔══██╗   ██╔╝                                           
███████║╚██████╔╝██████╔╝   ██║                                            
╚══════╝ ╚═════╝ ╚═════╝    ╚═╝                                            
                                                                           
██╗  ██╗██████╗ ██████╗ ██╗  ██╗██╗██╗     ██╗     ███████╗██████╗         
██║  ██║██╔══██╗██╔══██╗██║ ██╔╝██║██║     ██║     ██╔════╝██╔══██╗        
███████║██║  ██║██║  ██║█████╔╝ ██║██║     ██║     █████╗  ██████╔╝        
██╔══██║██║  ██║██║  ██║██╔═██╗ ██║██║     ██║     ██╔══╝  ██╔══██╗        
██║  ██║██████╔╝██████╔╝██║  ██╗██║███████╗███████╗███████╗██║  ██║        
╚═╝  ╚═╝╚═════╝ ╚═════╝ ╚═╝  ╚═╝╚═╝╚══════╝╚══════╝╚══════╝╚═╝  ╚═╝        
                                                                           
███████╗██╗  ██╗████████╗██████╗  █████╗  ██████╗████████╗ ██████╗ ██████╗ 
██╔════╝╚██╗██╔╝╚══██╔══╝██╔══██╗██╔══██╗██╔════╝╚══██╔══╝██╔═══██╗██╔══██╗
█████╗   ╚███╔╝    ██║   ██████╔╝███████║██║        ██║   ██║   ██║██████╔╝
██╔══╝   ██╔██╗    ██║   ██╔══██╗██╔══██║██║        ██║   ██║   ██║██╔══██╗
███████╗██╔╝ ██╗   ██║   ██║  ██║██║  ██║╚██████╗   ██║   ╚██████╔╝██║  ██║
╚══════╝╚═╝  ╚═╝   ╚═╝   ╚═╝  ╚═╝╚═╝  ╚═╝ ╚═════╝   ╚═╝    ╚═════╝ ╚═╝  ╚═╝
                                               by illwill                                                                                                    
""")

import pefile
import subprocess
import os

def decompress_upx_packed_executable(upx_packed_file, output_file):
    try:
        # Run the UPX decompression command and discard the output
        with open('/dev/null', 'w') as null_out:
            upx_process = subprocess.Popen(["upx", "-d", upx_packed_file, "-o", output_file], stdout=null_out, stderr=null_out)
            upx_process.wait()
        
        # Check the return code to see if UPX decompression was successful
        if upx_process.returncode == 0:
            return True
        else:
            print(f"[!] UPX decompression failed with return code {upx_process.returncode}")
            return False
    except Exception as e:
        print(f"[!] UPX decompression failed: {e}")
        return False


def extract_resource(pe, resource_name, output_file):
    try:
        resource_entry = None
        for resource_type in pe.DIRECTORY_ENTRY_RESOURCE.entries:
            for resource_id in resource_type.directory.entries:
                if resource_id.name is not None:
                    name = resource_id.name.decode('utf-8')
                    if name == resource_name:
                        resource_entry = resource_id
                        break

        if resource_entry is not None:
            resource_data_rva = resource_entry.directory.entries[0].data.struct.OffsetToData
            resource_data_size = resource_entry.directory.entries[0].data.struct.Size
            resource_data = pe.get_memory_mapped_image()[resource_data_rva:resource_data_rva + resource_data_size]
            
            with open(output_file, 'wb') as output:
                output.write(resource_data)
            
            return True
        else:
            return False
    except Exception as e:
        print("Error:", str(e))
        return False

def find_and_extract_resource(binary_file, resource_name, output_file):
    try:

        # Create a temporary file for the decompressed binary
        temp_binary_file = "temp_binary.exe"
        
        # Decompress the UPX-packed binary
        if decompress_upx_packed_executable(binary_file, temp_binary_file):
            print("[+] UPX decompression of Subseven.exe successful.")

            pe = pefile.PE(temp_binary_file)
            if extract_resource(pe, resource_name, output_file):
                print(f"[+] XOR ciphered resource '{resource_name}' successfully extracted to '{output_file}'.")
            else:
                print(f"[!] Resource '{resource_name}' not found or extraction failed.")
            # Clean up the temporary binary file
            os.remove(temp_binary_file)
#            os.remove(output_file)
        else:
            print("[!] UPX decompression failed.")
    except Exception as e:
        print("[!] Error:", str(e))


def decrypt(ce):
    x = 0
    PW = "5075579"
    decrypted_text = ''

    for char in ce:
        npw = ord(PW[x])
        if x == len(PW) -2:
            npw = 0
        decrypted_char = ord(char) - npw
        # Ensure the result is within the valid ASCII range (0-255)
        if 0 <= decrypted_char <= 0x110000:
            decrypted_text += chr(decrypted_char)
        x += 1
        if x == len(PW) -1:
            x = 0

    return decrypted_text

def decrypt_and_write_to_file(input_file, output_file):
    with open(input_file, 'r', encoding='latin-1') as file:
        with open(output_file, 'w', encoding='utf-8') as decrypted_file:
            for line in file:
                decrypted_line = decrypt(line.strip())
                decrypted_file.write(decrypted_line + '\n')

if __name__ == "__main__":
    binary_file = "SubSeven.exe"
    resource_name = "RCDATA_3"
    output_file = "RCDATA_3.bin"
    decrypted_output_file = "hddkiller.bat"
    find_and_extract_resource(binary_file, resource_name, output_file)
    decrypt_and_write_to_file(output_file, decrypted_output_file)
    os.remove(output_file)
    print(f"[+] Successfully decrypted '{decrypted_output_file}' to current directory. Using key: '5075579'.")
